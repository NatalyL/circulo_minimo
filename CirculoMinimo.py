# Programa que calcula el circulo minimo en una nube de puntos
import math
import matplotlib.pyplot as plt
import random
from math import sqrt

# Definimos el infinito
INF = 10**18
# Fadio del circulo (limite de posicion de puntos)
circle_r = 450
ptlist = []

# Funcion para colocar los puntos aleatoriamente en la pantalla
def randomPoint(N):
    for i in range(int(N)):
        # angulo de valor randomico
        alpha = 2 * math.pi * random.random()
        # radio randomico
        r = circle_r * random.random()
        # coordenadas del punto x, y
        x = int(r * math.cos(alpha) + circle_r) + 20
        y = int(r * math.sin(alpha) + circle_r) + 20
        if not ([x, y] in ptlist):
            ptlist.append([x, y]) # agrega los puntos en ptlist

# Función para devolver la distancia euclidiana entre dos puntos
def dist(a, b):
    return sqrt(pow(a[0] - b[0], 2) + pow(a[1] - b[1], 2))

# Función para comprobar si un punto se encuentra dentro de los límites del círculo
def is_inside(c, p):
    return dist(c[0], p) <= c[1]

# Las siguientes dos funciones son las funciones utilizadas para encontrar la ecuación del círculo cuando se dan 3 puntos.

# Método auxiliar para obtener un círculo definido por 3 puntos
def get_circle_center(bx, by, cx, cy):
    B = bx * bx + by * by
    C = cx * cx + cy * cy
    D = bx * cy - by * cx
    return [(cy * B - by * C) // (2 * D),
            (bx * C - cx * B) // (2 * D)]

# Función para devolver un círculo único que interseca tres puntos
def circle_frOm(A, B, C):
    I = get_circle_center(B[0] - A[0], B[1] - A[1],
                          C[0] - A[0], C[1] - A[1])
    I[0] += A[0]
    I[1] += A[1]
    return [I, dist(I, A)]


# Función para devolver el círculo más pequeño que interseca 2 puntos
def circle_from(A, B):
    # Establece el centro como el punto medio de A y B
    C = [(A[0] + B[0]) / 2.0, (A[1] + B[1]) / 2.0]
    # Establece el radio a la mitad de la distancia AB
    return [C, dist(A, B) / 2.0]

# Función para comprobar si un círculo encierra los puntos dados
def is_valid_circle(c, P):
    # Iterando todos los puntos para comprobar si los puntos se encuentran dentro del círculo
    for p in P:
        if (is_inside(c, p) == False):
            return False
    return True

# Función para encontrar el círculo mínimo de un conjunto de puntos
def minimum_enclosing_circle(P):
    # Encontramos el número de puntos
    n = len(P)
    if (n == 0):
        return [[0, 0], 0]
    if (n == 1):
        return [P[0], 0]

    # Establecemos MEC (Circulo Envolvente Minimo) inicial para que tenga un radio infinito
    mec = [[0, 0], INF]

    # Repasa todos los pares de puntos
    for i in range(n):
        for j in range(i + 1, n):
            # Obtenemos el círculo más pequeño que interseca a P [i] y P [j]
            tmp = circle_from(P[i], P[j])
            # Actualiza MEC si tmp encierra todos los puntos y tiene un radio más pequeño
            if (tmp[1] < mec[1] and is_valid_circle(tmp, P)):
                mec = tmp

    # Repasa todos los triples de puntos
    for i in range(n):
        for j in range(i + 1, n):
            for k in range(j + 1, n):
                # Obtenemos el círculo que interseca a P [i], P [j], P [k]
                tmp = circle_frOm(P[i], P[j], P[k])
                # Actualiza MEC si tmp encierra todos los puntos y tiene un radio más pequeño
                if (tmp[1] < mec[1] and is_valid_circle(tmp, P)):
                    mec = tmp

    return mec

def main():
    # Ingresamos el valor de N
    N = input("Introduce N : ")
    # Colocacion de los puntos de manera aleatoria
    randomPoint(N)
    # Introduccion de todos los valores de ptlist en la funcion del Circulo minimo
    mec = minimum_enclosing_circle(ptlist)
    # Impresion del centro y el radio del circulo
    print("Centro = {", mec[0][0], ",", mec[0][1], "} Radio = ", round(mec[1], 6))
    # Trazamos un circulo sin color de relleno
    figure, axes = plt.subplots()
    draw_circle = plt.Circle((mec[0][0], mec[0][1]), mec[1], fill=False)
    axes.set_aspect(1)
    axes.add_artist(draw_circle)
    # Graficacion de los puntos (color rojo)
    for [x, y] in ptlist:
        plt.plot([x], [y], 'r.', lw=0.1)
    # Añadimos un titulo
    plt.suptitle('Circulo Minimo')
    # Damos tamaño a los ejes e imprimimos
    plt.axis([0, 1200, 0, 1200])
    plt.show()

# llamamos al metodo main
main()